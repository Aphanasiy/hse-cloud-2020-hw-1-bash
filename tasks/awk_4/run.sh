#!/bin/bash

awk '{
    x+=1
    ans = ans sprintf("%s %s %s %s", $1, $2, $3, $4)
    if (x % 2 == 0) {
        print ans
        ans = ""
    } else
        ans = sprintf("%s%s", ans, ";")
    #print "<<<" ans
}'

#!/bin/bash

init=0

x=1
read ans

while read line
do
	if [ $(($x % 3)) == 0 ]
	then
		printf "${ans}\n"
		ans="${line}"
	else
		ans="${ans}\t${line}"
	fi
	x=$(($x+1))
done
printf "${ans}\n"

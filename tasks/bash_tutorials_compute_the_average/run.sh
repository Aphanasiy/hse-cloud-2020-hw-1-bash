#!/bin/bash

read N

sum=$(awk '{sum+=$1} END {print sum}')
printf "%.3f\n" $(echo "scale=4;" $sum/$N | bc)

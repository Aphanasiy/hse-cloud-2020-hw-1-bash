#!/bin/bash

read a
read b
read c


if [ $a -eq $b -a $a -eq $c -a $b -eq $c ]
then
	echo "EQUILATERAL"
	exit 0
fi


if [ $a -eq $b -o $a -eq $c -o $b -eq $c ]
then
	echo "ISOSCELES"
	exit 0
fi

if [ $a -ne $b -a $a -ne $c -a $b -ne $c ]
then
	echo "SCALENE"
	exit 0
fi
